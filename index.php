<?php
include("MySplitter.php");

if(isset($_POST["host"]) && isset($_POST["databaseName"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["databaseType"]))
{
    if(isset($_POST["action"]))
    {
        if($_POST["action"] == "getTables")
        {
            $res = getTables($_POST["host"], $_POST["databaseName"], $_POST["username"], $_POST["password"], $_POST["databaseType"]);
            echo $res;
        }
    }
    else
    {
        $res = checkConnexion($_POST["host"], $_POST["databaseName"], $_POST["username"], $_POST["password"], $_POST["databaseType"]);
        echo $res;
    }
}

?>
