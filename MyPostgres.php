<?php

function getConnexionPostgre($host, $databaseName, $username, $password)
{
    return new PDO("pgsql:host=" . $host . ";dbname=" . $databaseName, $username, $password);
}

function testConnexionPostgre($host, $databaseName, $username, $password)
{
    try {
        $connexion = getConnexionPostgre($host, $databaseName, $username, $password);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $res = "true";
    }
    catch (PDOException $e)
    {
        $res = "false";
    }

    return $res;
}

function getTablePostgre($host, $databaseName, $username, $password)
{
    $connexion = getConnexionPostgre($host, $databaseName, $username, $password);
    $req = $connexion->query("select table_name from information_schema.tables where table_schema = '$username';");
    $tables = $req->fetchAll();

    $res = '{"Tables": [';
    for($i = 0; $i < sizeof($tables); $i++)
    {
        $res .= '{"Name":"' . $tables[$i][0] .'",';
        $res .= '"Columns":[';
        $columns = getColumnPostgre($connexion, $tables[$i][0]);


        for($j = 0; $j < sizeof($columns); $j++)
        {
            $res .= '{';
            $res .= '"Name": "' . $columns[$j][0] . '",';
            $res .= '"Type": "' . $columns[$j][1] . '",';
            $res .= '"Nullable": "' . $columns[$j][2] . '"';
            $res .= '}';
            if($j < sizeof($columns) - 1)
            {
                $res .= ',';
            }
        }

        $res .= "]}";
        if($i < sizeof($tables) - 1)
        {
            $res .= ",";
        }
    }

    $res .= "]}";

    return $res;
}

function getColumnPostgre($connexion, $tableName)
{
    $req = $connexion->query("SELECT column_name, data_type, is_nullable FROM information_schema.columns WHERE table_name = '" . $tableName . "';");
    $columns = $req->fetchAll();
    return $columns;
}

?>
