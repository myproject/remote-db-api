<?php

function getConnexionMariadb($host, $databaseName, $username, $password)
{
    return $connexion = new PDO("mysql:host=" . $host . ";dbname=" . $databaseName, $username, $password);
}

function testConnexionMariadb($host, $databaseName, $username, $password)
{
    try {
        $connexion = getConnexionMariadb($host, $databaseName, $username, $password);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $res = "true";
    }
    catch (PDOException $e)
    {
        $res = "false";
    }

    return $res;
}

function getTableMariadb($host, $databaseName, $username, $password)
{
    $connexion = getConnexionMariadb($host, $databaseName, $username, $password);
    $req = $connexion->query("show tables;");
    $tables = $req->fetchAll();

    $res = '{"Tables": [';
    for($i = 0; $i < sizeof($tables); $i++)
    {
        $res .= '{"Name":"' . $tables[$i][0] .'",';
        $res .= '"Columns":[';
        $columns = getColumnMariadb($connexion, $tables[$i][0]);


        for($j = 0; $j < sizeof($columns); $j++)
        {
            $res .= '{';
            $res .= '"Name": "' . $columns[$j][0] . '",';
            $res .= '"Type": "' . $columns[$j][1] . '",';
            $res .= '"Nullable": "' . $columns[$j][2] . '",';
            $res .= '"Key": "' . $columns[$j][3] . '",';
            $res .= '"Default": "' . $columns[$j][4] . '",';
            $res .= '"Extra": "' . $columns[$j][5] . '"';
            $res .= '}';
            if($j < sizeof($columns) - 1)
            {
                $res .= ',';
            }
        }

        $res .= "]}";
        if($i < sizeof($tables) - 1)
        {
            $res .= ",";
        }
    }

    $res .= "]}";

    return $res;
}

function getColumnMariadb($connexion, $tableName)
{
    $req = $connexion->query("show columns from " . $tableName. ";");
    $columns = $req->fetchAll();
    return $columns;
}