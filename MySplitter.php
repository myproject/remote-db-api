<?php

include("MyPostgres.php");
include("MyMySql.php");

function checkConnexion($host, $databaseName, $username, $password, $databaseType)
{
    $res = "false";
    if($databaseType == "postgre")
    {
        $res = testConnexionPostgre($host, $databaseName, $username, $password);
    }
    else if($databaseType == "mariasql")
    {
        $res = testConnexionMariadb($host, $databaseName, $username, $password);
    }
    return $res;
}

function getTables($host, $databaseName, $username, $password, $databaseType)
{
    $res = "";
    if($databaseType == "postgre")
    {
        $res = getTablePostgre($host, $databaseName, $username, $password);
    }
    else if($databaseType == "mariasql")
    {
        $res = getTableMariadb($host, $databaseName, $username, $password);
    }

    return $res;
}

?>